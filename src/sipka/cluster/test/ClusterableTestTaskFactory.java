package sipka.cluster.test;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import saker.build.runtime.execution.ExecutionContext;
import saker.build.task.ParameterizableTask;
import saker.build.task.Task;
import saker.build.task.TaskContext;
import saker.build.task.TaskFactory;
import saker.build.task.identifier.TaskIdentifier;
import saker.build.thirdparty.saker.util.ImmutableUtils;
import saker.nest.utils.FrontendTaskFactory;

public class ClusterableTestTaskFactory extends FrontendTaskFactory<Object> {
	private static final long serialVersionUID = 1L;

	@Override
	public ParameterizableTask<? extends Object> createTask(ExecutionContext executioncontext) {
		return new ParameterizableTask<Object>() {
			@Override
			public Object run(TaskContext taskcontext) throws Exception {
				Map<String, String> userparams = taskcontext.getExecutionContext().getEnvironment().getUserParameters();
				System.out.println("saker.build.environment.name: " + userparams.get("saker.build.environment.name"));
				for (int i = 0; i < 100; i++) {
					ClusterWorkerTaskFactory worker = new ClusterWorkerTaskFactory(UUID.randomUUID().toString());
					taskcontext.startTask(worker, worker, null);
				}
				return null;
			}
		};
	}

	public static class ClusterWorkerTaskFactory
			implements TaskFactory<Object>, Task<Object>, TaskIdentifier, Externalizable {
		private static final long serialVersionUID = 1L;

		private String str;

		/**
		 * For {@link Externalizable}.
		 */
		public ClusterWorkerTaskFactory() {
		}

		public ClusterWorkerTaskFactory(String str) {
			this.str = str;
		}

		@Override
		public Task<? extends Object> createTask(ExecutionContext executioncontext) {
			return this;
		}

		@Override
		public Set<String> getCapabilities() {
			return ImmutableUtils.makeImmutableNavigableSet(new String[] { CAPABILITY_REMOTE_DISPATCHABLE });
		}

		@Override
		public int getRequestedComputationTokenCount() {
			return 1;
		}

		@Override
		public Object run(TaskContext taskcontext) throws Exception {
			Map<String, String> userparams = taskcontext.getExecutionContext().getEnvironment().getUserParameters();
			String envname = userparams.get("saker.build.environment.name");
			taskcontext.println("Running on env: " + envname + " with " + str);
			Thread.sleep(4000);
			return str;
		}

		@Override
		public void writeExternal(ObjectOutput out) throws IOException {
			out.writeObject(str);
		}

		@Override
		public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
			str = (String) in.readObject();
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((str == null) ? 0 : str.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ClusterWorkerTaskFactory other = (ClusterWorkerTaskFactory) obj;
			if (str == null) {
				if (other.str != null)
					return false;
			} else if (!str.equals(other.str))
				return false;
			return true;
		}
	}
}
